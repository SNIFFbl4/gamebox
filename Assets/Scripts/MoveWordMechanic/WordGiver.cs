using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameBoxProject
{
    public class WordGiver : MonoBehaviour
    {
        private static Dictionary<string, WordType> _wordsToGive = new();
        private static AllWords _otherWordsStatic;

        private void Awake()
        {
            Initialize(SceneContent.Instance._obligatoryWords);

            _otherWordsStatic = SceneContent.Instance._otherWords;

            EnemyWord.OnWordDead += DeleteWord;
            WordPanel.OnBuffWordComplete += DeleteWord;
        }

        public static bool TryGetWord(out WordType type, out string word)
        {
            if (_wordsToGive.Count == 0)
            {
                GetRandomOtherWord(out word, out type);
                return true;
            }

            List<string> keys = _wordsToGive.Keys.ToList();
            int randIndex = Random.Range(0, _wordsToGive.Count);

            word = keys[randIndex];
            type = _wordsToGive[word];

            return true;
        }

        private static void GetRandomOtherWord(out string word, out WordType type)
        {
            int randTypeIndex = Random.Range(0, _otherWordsStatic.Words.Count);
            type = _otherWordsStatic.Words[randTypeIndex].Type;

            int randWordIndex = Random.Range(0, _otherWordsStatic.Words[randTypeIndex].Words.Count);
            word = _otherWordsStatic.Words[randTypeIndex].Words[randWordIndex];
        }

        public static bool TryGetWordNecessary(List<string> usedWords, out WordType type, out string word)
        {
            foreach (var givenWord in _wordsToGive.Keys)
            {
                if (usedWords.Contains(givenWord) is false)
                {
                    type = _wordsToGive[givenWord];
                    word = givenWord;
                    return true;
                }
            }

            GetRandomOtherWord(out word, out type);
            while (usedWords.Contains(word))
            {
                GetRandomOtherWord(out word, out type);
            }
            return true;
        }

        private void DeleteWord(string word, WordType type)
        {
            if (_wordsToGive.Count == 0)
                return;

            if (_wordsToGive.ContainsKey(word))
                _wordsToGive.Remove(word);
        }

        private static void Initialize(AllWords listWithWords)
        {
            foreach (var wordList in listWithWords.Words)
            {
                foreach (var word in wordList.Words)
                {
                    if (_wordsToGive.ContainsKey(word))
                        continue;

                    _wordsToGive.Add(word, wordList.Type);
                }
            }
            Debug.Log($"{_wordsToGive.Count} words are added to base");
        }
    }
}