using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameBoxProject
{
    public class SelectedWordsHolder : MonoBehaviour
    {
        public event Action<List<string>> OnWordsChanged;

        [SerializeField] private TextPlank secretWordPrefab;
        [Header("AUDIO")]
        [SerializeField] private AudioSource _audio;
        [SerializeField] private AudioClip _niceChoiceSound;
        [SerializeField] private AudioClip _badChoiceSound;
        
        private WordsColors _wordColor;
        private TextPlank[] _planks;

        public bool ResultIsReady { get; private set; }

        public void Init(List<string> words)
        {
            ResultIsReady = false;
            _wordColor = SceneContent.Instance._wordsColors;

            _planks = new TextPlank[words.Count];
            WordMover.OnWordEndDrag += Refresh;

            for (int i = 0; i < words.Count; i++)
            {
                _planks[i] = Instantiate(secretWordPrefab, transform);
                var color = _wordColor.WordColors.Find(x => x.WordType == WordType.any).Color;
                _planks[i].Init(color, "---�����---", WordType.any);
                var secret = _planks[i].gameObject.AddComponent<SecretWordHolder>();
                secret.Init(words[i], WordType.any);
                _planks[i].gameObject.GetComponent<WordMover>().enabled = false;
            }
        }

        private void Refresh()
        {
            OnWordsChanged?.Invoke(GetWords());
        }

        private List<string> GetWords()
        {
            List<string> result = new List<string>();
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.activeInHierarchy is false)
                {
                    continue;
                }

                else if (transform.GetChild(i).TryGetComponent(out TextPlank plank))
                {
                    Debug.Log($"Get word [{plank}] with type [{plank.WordType}]");
                    if (plank.WordType == WordType.any)
                        result.Add(string.Empty);
                    else
                        result.Add(plank.ToString());
                }
            }
            Debug.Log($"Return List with {result.Count} elements");

            return result;
        }

        private void OnDestroy()
        {
            WordMover.OnWordEndDrag -= Refresh;
        }

        internal void CheckResult(List<string> secretWords, int currentChildIndex = 0, int secretWordIndex = 0)
        {
            if (currentChildIndex >= transform.childCount)
            {
                ResultIsReady = true;
                return;
            }

            if (transform.GetChild(currentChildIndex).gameObject.activeInHierarchy is false)
            {
                CheckResult(secretWords, currentChildIndex + 1, secretWordIndex);
                return;
            }

            if (transform.GetChild(currentChildIndex).TryGetComponent(out TextPlank plank))
            {
                if (plank.WordType == WordType.any)
                {
                    CheckResult(secretWords, currentChildIndex + 1, secretWordIndex + 1);
                    return;
                }

                if (plank.ToString().Equals(secretWords[secretWordIndex]))
                {
                    Debug.Log($"Word {plank} equals to {secretWords[secretWordIndex]}");
                    _audio.Stop();
                    _audio.PlayOneShot(_niceChoiceSound);
                    plank.SetWordColor(Color.green, () => CheckResult(secretWords, currentChildIndex + 1, secretWordIndex + 1));
                }
                else
                {
                    Debug.Log($"Word {plank} NOT equals to {secretWords[secretWordIndex]}");
                    _audio.Stop();
                    _audio.PlayOneShot(_badChoiceSound);
                    plank.SetWordColor(Color.red, () => CheckResult(secretWords, currentChildIndex + 1, secretWordIndex + 1));
                }
            }
        }
    }
}