using GameBoxProject;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.MenuAndUI
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private GameObject _mainMenuPanel;
        [SerializeField] private GameObject _intermediateMenuPanel;
        [SerializeField] private GameObject _aboutAuthorsPanel;

        [SerializeField] private Button _prologButton;
        [SerializeField] private Button _gameButton;
        [SerializeField] private Button _aboutAuthorsButton;
        [SerializeField] private Button _settingsButton; //�����������

        [SerializeField] private Button _shopButton;
        [SerializeField] private Button _levelSelectionButton;
        [SerializeField] private Button _diariesButton;

        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _backButton;

        private void Awake()
        {
            _aboutAuthorsPanel.SetActive(false);
            _intermediateMenuPanel.SetActive(false);
            _backButton.gameObject.SetActive(false);
            _diariesButton.gameObject.SetActive(false); //���� �� ������ ��������

            _prologButton.onClick.AddListener(() => SceneController.LoadSceneWithFade(Scenes.PrologueScene));

            _aboutAuthorsButton.onClick.AddListener(() => OpenAboutAuthorsPanel());
            _settingsButton.onClick.AddListener(() => StartCoroutine(LoadSceneViaPause(Scenes.SettingsScene)));   //SceneController.LoadScene(Scenes.SettingsScene));
            
            _shopButton.onClick.AddListener(() => StartCoroutine(LoadSceneViaPause(Scenes.ShopScene)));           //SceneController.LoadScene(Scenes.ShopScene));

            _gameButton.onClick.AddListener(() => OpenIntermediateMenuPanel());
            
            _levelSelectionButton.onClick.AddListener(() => StartCoroutine(LoadSceneViaPause(Scenes.LevelSelectionScene))); //SceneController.LoadScene(Scenes.LevelSelectionScene));

            _exitButton.onClick.AddListener(() => Invoke(nameof(Exit), 0.2f));
            _backButton.onClick.AddListener(() => BackToMainMenu());
        }

        private IEnumerator LoadSceneViaPause(Scenes sceneName)
        {
            yield return new WaitForSeconds(0.2f);
            
            switch (sceneName)
            {
                case Scenes.SettingsScene:
                    SceneController.LoadScene(Scenes.SettingsScene);
                    break;
                case Scenes.ShopScene:
                    SceneController.LoadScene(Scenes.ShopScene);
                    break;
                case Scenes.LevelSelectionScene:
                    SceneController.LoadScene(Scenes.LevelSelectionScene);
                    break;
                default:
                    Debug.Log("������ �������� �����");
                    break;
            }
        }

        /// <summary>
        /// �������� �����, ����������� ��������������� ������ ����.
        /// </summary>
        private void OpenAboutAuthorsPanel()
        {
            _mainMenuPanel.SetActive(false);
            _intermediateMenuPanel.SetActive(false);
            _aboutAuthorsPanel.SetActive(true);
            _backButton.gameObject.SetActive(true);
        }
        
        /// <summary>
        /// ������� ������ �������������� ����.
        /// </summary>
        private void OpenIntermediateMenuPanel()
        {
            _intermediateMenuPanel.SetActive(true);
            _aboutAuthorsPanel.SetActive(false);
            _mainMenuPanel.SetActive(false);
            _backButton.gameObject.SetActive(true);
        }
        
        /// <summary>
        /// ������� � ������� ����.
        /// </summary>
        private void BackToMainMenu()
        {
            _intermediateMenuPanel.SetActive(false);
            _aboutAuthorsPanel.SetActive(false);
            _mainMenuPanel.SetActive(true);
            _backButton.gameObject.SetActive(false);
        }

        private void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}

