﻿using Assets.Scripts.Datas;
using Assets.Scripts.Units.Character;
using DG.Tweening;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


namespace Assets.Scripts.MenuAndUI
{
    public class DamageVignetteController : MonoBehaviour
    {
        [SerializeField] private DamageVignetteData _data;
        [SerializeField] private Image _leftTopImage;
        [SerializeField] private Image _rightBottomImage;

        private void Awake()
        {
            CharacterDamageController.TakeDamageEvent += EnableVignette;
        }

        private void OnDestroy()
        {
            CharacterDamageController.TakeDamageEvent -= EnableVignette;
        }

        private void EnableVignette(MainCharacterController mainCharacter)
        {
            float riseDuration = _data.AlphaRiseDuration;
            float decreaseDuration = _data.AlphaDecreaseDuration;
            
            float endAlpha = AlphaCalculate(mainCharacter);

            _leftTopImage.gameObject.SetActive(true);
            _rightBottomImage.gameObject.SetActive(true);

            var sequenceOne = DOTween.Sequence();
            sequenceOne.Append(_leftTopImage.DOFade(endAlpha, riseDuration))
                .Append(_leftTopImage.DOFade(0, decreaseDuration))
                .SetLink(gameObject, LinkBehaviour.KillOnDestroy);

            var sequenceTwo = DOTween.Sequence();
            sequenceTwo.Append(_rightBottomImage.DOFade(endAlpha, riseDuration))
                .Append(_rightBottomImage.DOFade(0, decreaseDuration))
                .OnComplete(DisableVignette)
                .SetLink(gameObject, LinkBehaviour.KillOnDestroy);
        }

        private float AlphaCalculate(MainCharacterController mainCharacter)
        {
            float currentHP = mainCharacter.CurrentHP;
            float maxHP = mainCharacter.Health.MaxHP;
            float alphaMinValue = _data.AlphaMinValue;

            float endAlpha = (100 - (currentHP * 100 / maxHP)) / 100;
            if (endAlpha < alphaMinValue)
                endAlpha = alphaMinValue;

            return endAlpha;
        }

        private void DisableVignette()
        {
            _leftTopImage.gameObject.SetActive(false);
            _rightBottomImage.gameObject.SetActive(false);
        }
    }
}
