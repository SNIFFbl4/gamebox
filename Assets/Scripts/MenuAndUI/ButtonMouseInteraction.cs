﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Assets.Scripts.MenuAndUI
{
    public class ButtonMouseInteraction : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
    {
        public static event Action OnMouseEnter;
        public static event Action<AudioClip> OnButtonClicked;

        [SerializeField] private AudioClip _clickSound;
        [SerializeField] private GameObject _frame;

        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();

            _frame?.SetActive(false);
            
            if (_clickSound == null)
                _clickSound = Resources.Load<AudioClip>("ButtonClickStandartSound");
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _frame?.SetActive(true);
            OnMouseEnter.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _frame?.SetActive(false);
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            OnButtonClicked.Invoke(_clickSound);
        }

        
    }
}
