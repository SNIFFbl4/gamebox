using UnityEngine;

namespace GameBoxProject
{
    public class EnemyDeadSoundAdapter : MonoBehaviour
    {
        [SerializeField] private AudioSource _audio;
        [SerializeField] private AudioClip _clip;

        private void Start()
        {
            Enemy.OnEnemyDead += Enemy_OnEnemyDead;
        }

        private void Enemy_OnEnemyDead()
        {
            _audio.PlayOneShot(_clip);
        }

        private void OnDestroy()
        {
            Enemy.OnEnemyDead -= Enemy_OnEnemyDead;
        }
    }
}