using UnityEngine;

namespace GameBoxProject
{
    public class GameOverSoundsAdapter : MonoBehaviour
    {
        [SerializeField] private AudioSource _audio;
        [SerializeField] private AudioClip _clip;

        private void Start()
        {
            EndGamePanels.OnGameEnded += EndGamePanels_OnGameEnded;
        }

        private void EndGamePanels_OnGameEnded(bool isWin)
        {
            
        }

        private void OnDestroy()
        {
            EndGamePanels.OnGameEnded -= EndGamePanels_OnGameEnded;
        }
    }
}