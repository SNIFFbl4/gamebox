using Assets.Scripts.SaveLoadData;
using UnityEngine;
using UnityEngine.Audio;

namespace GameBoxProject
{
    public class LoadSoundSettings : MonoBehaviour
    {
        private const string MASTER = "MasterVolume";
        private const string SOUNDS = "SoundsVolume";
        private const string MUSIC = "MusicVolume";

        [SerializeField] private AudioMixer _audioMixer;

        private void Start()
        {
            var loadData = new LoadData();

            float masterVolume = loadData.GetFloatData(GlobalVariables.MasterVolume);
            float soundsVolume = loadData.GetFloatData(GlobalVariables.SoundsVolume);
            float musicVolume = loadData.GetFloatData(GlobalVariables.MusicVolume);

            ChangeMixerGroup(MASTER, masterVolume);
            ChangeMixerGroup(SOUNDS, soundsVolume);
            ChangeMixerGroup(MUSIC, musicVolume);

            Debug.Log($"Master={masterVolume}. Music={musicVolume}. Sounds={soundsVolume}");
        }

        private void ChangeMixerGroup(string groupName, float value) =>
            _audioMixer.SetFloat(groupName, value);
    }
}