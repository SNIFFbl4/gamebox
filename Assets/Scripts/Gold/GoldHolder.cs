using Assets.Scripts.SaveLoadData;
using Assets.Scripts.Units.Character;
using UnityEngine;

namespace GameBoxProject
{
    public class GoldHolder : MonoBehaviour
    {
        public Gold Gold { get; private set; }

        private int _startGold = 0;

        private void Awake()
        {
            Construct();
        }

        private void GameOver(bool isWin)
        {
            int result = Gold.Current - _startGold;
            Debug.Log($"Coins: start [{_startGold}]; now [{Gold.Current}]; result to save [{result}]");
            Observer.SaveCoinsEvent(result);
        }

        public void AddGold(int value)
        {
            Gold.AddGold(value);
        }

        private void Construct() 
        {
            _startGold = new LoadData().GetIntData(GlobalVariables.Coins);
            Gold = new Gold(_startGold);
            Debug.Log($"App start. Start coins = {_startGold}");
            EndGamePanels.OnGameEnded += GameOver;
        }

        private void OnDestroy()
        {
            EndGamePanels.OnGameEnded -= GameOver;
        }
    }
}