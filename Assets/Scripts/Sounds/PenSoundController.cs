﻿using UnityEngine;

namespace Assets.Scripts.Sounds
{
    internal class PenSoundController
    {
        private AudioSource _hit;
        private AudioSource _shot;
        private AudioSource _deadHit;

        internal PenSoundController(Transform penTransform)
        {
            _hit = Resources.Load<AudioSource>("PenHitSound");
            _shot = Resources.Load<AudioSource>("PenShotSound");

            _hit = Object.Instantiate(_hit, penTransform);
            _shot = Object.Instantiate(_shot, penTransform);

            _deadHit = Object.Instantiate(Resources.Load<AudioSource>("PencilEndHit"), penTransform);
        }

        public void Hit()
        {
            _hit.Play();
        }

        public void Shot()
        {
            _shot.Play();
        }

        public void DeadHit()
        {
            _deadHit.Play();
        }
    }
}
