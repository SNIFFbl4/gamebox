﻿using UnityEngine;

namespace Assets.Scripts.Sounds
{
    internal sealed class PencilSoundController
    {
        private AudioSource _hit;
        private AudioSource _whoosh;
        private AudioSource _deadHit;

        internal PencilSoundController(Transform pencilTransform)
        {
            _hit = Resources.Load<AudioSource>("PencilHitSound");
            _whoosh = Resources.Load<AudioSource>("PencilWhooshSound");

            _hit = Object.Instantiate(_hit, pencilTransform);
            _whoosh = Object.Instantiate(_whoosh, pencilTransform);

            _deadHit = Object.Instantiate(Resources.Load<AudioSource>("PencilEndHit"), pencilTransform);
        }

        public void Hit()
        {
            _hit.Play();
        }

        public void Whoosh()
        {
            _whoosh.Play();
        }

        internal void DeadHit()
        {
            _deadHit.Play();
        }
    }
}
