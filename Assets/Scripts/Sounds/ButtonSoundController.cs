﻿using Assets.Scripts.MenuAndUI;
using GameBoxProject;
using UnityEngine;

namespace Assets.Scripts.Sounds
{
    public class ButtonSoundController : MonoBehaviour
    {
        [SerializeField] private AudioSource _audio;
        [SerializeField] private AudioClip _mouseEnterSound;

        private void Awake()
        {
            ButtonMouseInteraction.OnMouseEnter += PlayMouseEnterSound;
            ButtonMouseInteraction.OnButtonClicked += PlayClickSound;
        }

        private void OnDestroy()
        {
            ButtonMouseInteraction.OnMouseEnter -= PlayMouseEnterSound;
            ButtonMouseInteraction.OnButtonClicked -= PlayClickSound;
        }

        private void PlayMouseEnterSound()
        {
            _audio.PlayOneShot(_mouseEnterSound);
        }
        
        private void PlayClickSound(AudioClip clip)
        {
            _audio.PlayOneShot(clip);
        }

        
    }
}
