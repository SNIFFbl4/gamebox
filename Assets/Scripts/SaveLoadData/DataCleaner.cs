using Assets.Scripts.SaveLoadData;
using UnityEngine;

namespace GameBoxProject
{
    public class DataCleaner : MonoBehaviour
    {
        private void Awake()
        {
            string savedGameVersion = new LoadData().GetStringData(GlobalVariables.GameVersion);
            string thisGameVersion = Application.version;

            if (thisGameVersion.Equals(savedGameVersion))
            {
                Debug.Log($"This version is {thisGameVersion}");
            }
            else
            {
                Debug.Log($"You have saved data from old version of game. Datas cleaned");
                PlayerPrefs.DeleteAll();
                new SaveData(GlobalVariables.GameVersion, Application.version);
            }
        }
    }
}