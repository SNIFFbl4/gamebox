using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace GameBoxProject
{
    public class ButtonWarning : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Button _button;
        [SerializeField] private string _warningText;
        [SerializeField] private TMP_Text _prefab;
        
        private TMP_Text _textUI;
        private bool _isShow = false;

        private void Awake()
        {
            _textUI = Instantiate(_prefab, _button.transform.root);
            _textUI.gameObject.SetActive(false);
            _textUI.text = _warningText;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (_button.interactable is false)
                ShowWarning();
            else if (_textUI != null)
                Destroy(_textUI.gameObject);
        }

        private void ShowWarning()
        {
            _textUI.gameObject.SetActive(true);
            _isShow = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (_button.interactable is false)
                HideWarning();
        }

        private void HideWarning()
        {
            _isShow = false;
            _textUI.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (_isShow)
            {
                var mousePos = Input.mousePosition + new Vector3(0, 30, 0);
                _textUI.transform.position = mousePos;
            }
        }
    }
}