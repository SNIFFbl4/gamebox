using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

namespace GameBoxProject
{
    public class TextPlank : MonoBehaviour
    {
        public event Action OnPlankScaled;

        [SerializeField] private Image _backImage;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private int _pxForSymbol;
        [SerializeField] private RectTransform _rectTransform;

        public SecretWordHolder SecretWordPlank { get; private set; }
        private string _word;
        private Vector3 _startScale;

        public WordType WordType { get; private set; }

        internal void Init(Color color, string word, WordType wordType, bool show = true)
        {
            gameObject.name = word;

            _word = word;
            WordType = wordType;
            _text.color = color;
            _startScale = transform.localScale;
            //_backImage.color = color;

            float width = _pxForSymbol * word.Length;

            if (show)
            {
                _rectTransform.sizeDelta = new Vector2(width, _rectTransform.sizeDelta.y);
                _text.text = word.ToUpper();
            }
            else
            {
                _rectTransform.sizeDelta = new Vector2(300f, _rectTransform.sizeDelta.y);
                _text.text = "";
            }
        }

        public void SetSecretPlank(SecretWordHolder secretWordHolder)
        {
            if (secretWordHolder != null)
            {
                SecretWordPlank = secretWordHolder;
                SecretWordPlank.SetTempWord(_word);
            }
        }

        public void ResetSecretPlank()
        {
            SecretWordPlank = null;
        }

        public void ShowSecretPlank()
        {
            if (SecretWordPlank != null)
            {
                SecretWordPlank.OnPlankDragged(this);
                SecretWordPlank.SetTempWord("");
                SecretWordPlank = null;
            }
        }

        public void SetWordColor(Color color, Action callback = null)
        {
            _text.DOColor(color, 0.1f).SetLink(_text.gameObject);
            transform.DOScale(_startScale * 1.3f, 0.1f).SetLink(gameObject).OnComplete(() =>
            {
                transform.DOScale(_startScale, 0.2f).SetLink(gameObject).OnComplete(() =>
                    callback?.Invoke());
            });
        }

        public override string ToString()
        {
            return _word;
        }
    }
}