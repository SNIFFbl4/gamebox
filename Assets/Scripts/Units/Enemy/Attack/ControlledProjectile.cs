﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace GameBoxProject
{
    public class ControlledProjectile : Projectile
    {
        [SerializeField]
        private GameObject _projectileView;
        [SerializeField]
        private GameObject _explosion;
        [SerializeField]
        private float _distanceAccuracy;

        private bool _isActive;
        private Vector2 _pointToGo;
        private float _xDistance;
        private float t;

        private Vector2 point1;
        private Vector2 point2;

        public override void Init(float damage)
        {
            _explosion.SetActive(false);
            _projectileView.SetActive(true);
            
            base.Init(damage);
            _isActive = true;
            _pointToGo = _target.position - new Vector3(0, 1.5f, 0);
            _xDistance = Mathf.Abs(transform.position.x - _pointToGo.x);

            t = 0;
            point1 = (Vector2)transform.position + (Vector2)transform.right * 10;
            point2 = new Vector2(point1.x, _target.position.y);
            
            Debug.Log($"Init bullet {name}! StartPos: {_startPosition}, PointToGo: {_pointToGo}");
        }

        protected override void TakeDamage(IDamagable damagable)
        {
            damagable.TakeDamage(_damage);
            Explosion();
        }
                

        private void Update()
        {
            if (_isActive is false)
                return;

            transform.position = GetDirection();
            
            if (Vector2.Distance(transform.position, _pointToGo) < _distanceAccuracy) 
            {
                Explosion();
                return;
            }
        }

        private Vector2 GetDirection()
        {
            t += Time.deltaTime * _speed;

            return Bezier.GetPoint(_startPosition, point1, point2, _pointToGo, t);
        }

        private void Explosion()
        {
            _isActive = false;
            
            DOTween.Sequence()
                   .AppendCallback(() => _projectileView.SetActive(false))
                   .AppendCallback(() => _explosion.SetActive(true))
                   .AppendInterval(1f)
                   .AppendCallback(() => BackToPool())
                   .SetLink(gameObject);
        }
    }
}