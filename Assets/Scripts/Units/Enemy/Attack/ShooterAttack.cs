﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameBoxProject
{
    public class ShooterAttack : EnemyAttack
    {
        public event Action OnSymbolStartAttack;
        
        [SerializeField] private WordAttackData _data;
        [SerializeField] private Transform[] _projectileSpawnPoints;
        [SerializeField] private Projectile _projectilePrefab;

        private Pool<Projectile> _pool;
        private WaitForSeconds _attackDelay;
        private WaitForSeconds _shotDelay;
        private Transform _target;

        public override void Init(float damage)
        {
            _pool = new Pool<Projectile>(_data.CountOfProjectiles, _projectilePrefab, true);
            base.Init(damage);
            _attackDelay = new WaitForSeconds(_data.TimeBetweenAttack);
            _shotDelay = new WaitForSeconds(_data.TimeBetweenShots);

            if (_target == null)
                _target = FindObjectOfType<MainCharacterController>().transform;
            
            StartCoroutine(PrepareToAttack());
        }

        private IEnumerator PrepareToAttack()
        {
            while (true) 
            {
                yield return _attackDelay;
                OnSymbolStartAttack?.Invoke();
                StartCoroutine(Attack());
            }
        }

        private IEnumerator Attack()
        {
            for (int i = 0; i < _projectileSpawnPoints.Length; i++) 
            {
                var bullet = _pool.GetObject();
                bullet.transform.SetParent(_projectileSpawnPoints[i]);
                bullet.transform.localPosition = Vector3.zero;
                bullet.transform.localRotation = Quaternion.identity;
                bullet.transform.SetParent(null);
                bullet.SetTarget(_target);
                bullet.Init(_damage);

                yield return _shotDelay;
            }
            yield break; 
        }
    }
}