﻿using System;
using System.Collections;
using UnityEngine;

namespace GameBoxProject
{
    public class Projectile : MonoBehaviour, IPoolObject
    {
        [SerializeField] protected float _speed;
        [SerializeField] protected Rigidbody2D _rigidbody2D;
        [SerializeField] protected float _timeToDestroy;
        [SerializeField] protected bool _needToDestroyAfterDelay = true;
        
        public event Action<IPoolObject> OnObjectNeededToDeactivate;

        protected float _damage;
        protected Transform _target;
        protected Vector2 _startPosition;
        
        protected virtual void TakeDamage(IDamagable component) {}

        public virtual void Init(float damage)
        {
            transform.SetParent(null);
            _damage = damage;
            
            if (_needToDestroyAfterDelay)
                StartCoroutine(WaitToDestroy());
        }

        public void ResetBeforeBackToPool()
        {
            gameObject.SetActive(false);
            _rigidbody2D.velocity = Vector2.zero;
            transform.localPosition = Vector2.zero;
            transform.localRotation = Quaternion.identity;
        }

        public void SetTarget(Transform target)
        {
            _target = target;
            _startPosition = transform.position;
        }

        private IEnumerator WaitToDestroy()
        {
            yield return new WaitForSeconds(_timeToDestroy);
            BackToPool();
        }
        
        protected void BackToPool()
        {
            Debug.Log($"Object {name} back to pool");
            OnObjectNeededToDeactivate?.Invoke(this);
        }
        
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out IDamagable damagable)) 
            {
                TakeDamage(damagable);
            }
        }
    }
}