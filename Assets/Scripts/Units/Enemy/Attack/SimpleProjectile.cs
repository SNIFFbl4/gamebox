﻿using System;
using System.Collections;
using UnityEngine;

namespace GameBoxProject
{
    internal class SimpleProjectile : Projectile
    {
        private void FixedUpdate()
        {
            _rigidbody2D.velocity = transform.up * _speed;
        }

        protected override void TakeDamage(IDamagable damagable)
        {
            damagable.TakeDamage(_damage);
            base.StopAllCoroutines();
            BackToPool();
        }
    }
}
