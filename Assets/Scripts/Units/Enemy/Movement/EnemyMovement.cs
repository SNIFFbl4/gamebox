using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameBoxProject
{
    public abstract class EnemyMovement : MonoBehaviour
    {
        [SerializeField] protected EnemyModel _enemyModel;

        protected Transform _target;
        protected bool _lookAtRight;
        protected bool _prevLook;

        public abstract void Construct(EnemyData data);

        public void SetTarget(Transform target)
        {
            _prevLook = !_lookAtRight;
            _target = target;
            LookAtPoint(_target.position);
        }

        protected bool IsTargetOnTheLeft(float xPointOfTarget)
        {
            if (xPointOfTarget < transform.position.x)
                return true;
            else
                return false;
        }

        public void StopMove()
        {
            _target = null;
        }

        protected void LookAtPoint(Vector3 lookPoint)
        {
            if (IsTargetOnTheLeft(lookPoint.x))
            {
                _lookAtRight = false;
            }
            else
            {
                _lookAtRight = true;
            }

            if (_prevLook != _lookAtRight) {
                _prevLook = _lookAtRight;
                RotateEnemy();
            }
        }

        protected virtual void RotateEnemy()
        {
            if (_lookAtRight) {
                transform.localScale = Vector3.one;
                _enemyModel.LookAtRight();
            } else {
                transform.localScale = new Vector3(-1, 1, 1);
                _enemyModel.LookAtLeft();
            }
        }
    }
}