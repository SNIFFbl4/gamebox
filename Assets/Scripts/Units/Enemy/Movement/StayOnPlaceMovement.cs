﻿using System;
using Unity.Mathematics;
using UnityEngine;

namespace GameBoxProject
{
    public class StayOnPlaceMovement : EnemyMovement
    {
        [SerializeField]
        private Transform[] points;
        
        public override void Construct(EnemyData data)
        {
        }

        private void FixedUpdate()
        {
            if (_target == null)
                return;

            LookAtPoint(_target.position); 
        }

        protected override void RotateEnemy()
        {
            base.RotateEnemy();
            if (IsTargetOnTheLeft(_target.position.x)) {
                foreach (var point in points) {
                    point.transform.rotation = Quaternion.Euler(0, 180f, 0);
                }
            } else {
                foreach (var point in points) {
                    point.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
            }
        }
    }
}