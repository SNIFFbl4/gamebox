﻿using GameBoxProject;
using System;
using UnityEngine;

namespace Assets.Scripts.Units.Character
{
    internal class CharacterAnimatorController
    {
        public static event Action LoseAnimationStartedEvent; 
        
        private Animator _animator;
        private Health _health;
        public CharacterAnimatorController(Animator animator, Health health) 
        {
            _health= health;
            
            _health.OnPersonDead += LoseAnimation;
            Observer.GameLevelFinishedEvent += Unsubscribe;
            
            _animator = animator;
        }

        internal void ChangeAnimation(bool isMove)
        {
            _animator.SetBool("IsMove", isMove);
        }

        private void LoseAnimation(object character)
        {
            LoseAnimationStartedEvent.Invoke();
            _animator.SetBool("IsLose", true);
        }

        private void Unsubscribe()
        {
            _health.OnPersonDead -= LoseAnimation;
            Observer.GameLevelFinishedEvent -= Unsubscribe;
        }
    }
}
