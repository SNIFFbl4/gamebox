﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Units.Character
{
    /// <summary>
    /// Висит на Character.
    /// </summary>
    public class CharacterDamageController : MonoBehaviour
    {
        public static event Action<MainCharacterController> TakeDamageEvent;
        
        [SerializeField] private AudioSource _audioSource;
        private bool _isDamageResistent;

        private void Awake()
        {
            MainCharacterController.CharacterAttackedEvent += DamageCalculate;
        }

        private void OnDestroy()
        {
            MainCharacterController.CharacterAttackedEvent -= DamageCalculate;
        }

        private void DamageCalculate(MainCharacterController mainCharacter, float damage, float duration)
        {
            if (CharacterAttributes.IsInvulnerAbilityActivated)
            {
                Debug.Log("Персонаж атакован. Действует способность Invulnerability. Урон = 0");
                return;
            }

            if (_isDamageResistent)
            {
                Debug.Log("Персонаж атакован. Действует DamageResistence. Урон = 0");
                return;
            }

            float armor = mainCharacter.Armor.CurrentArmor;
            var health = mainCharacter.Health;
            
            float resultDamage = damage - armor;
            if (resultDamage > 0)
            {
                _audioSource.Play();
                
                health.TakeDamage(resultDamage);
                
                TakeDamageEvent.Invoke(mainCharacter);

                Debug.Log($"Получен урон: {resultDamage}");
                Debug.Log($"Значение CurrentHP: {mainCharacter.CurrentHP}");

                _isDamageResistent = true;
                StartCoroutine(DamageResistenceTimer(duration));
            }
        }

        private IEnumerator DamageResistenceTimer(float duration)
        {
            yield return new WaitForSeconds(duration);
            _isDamageResistent = false;
            Debug.Log("Действие DamageResistence окончено.");
        }
    }
}
