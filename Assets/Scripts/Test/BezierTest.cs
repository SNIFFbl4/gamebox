﻿using System;
using Cinemachine.Utility;
using UnityEngine;

namespace GameBoxProject
{
    [ExecuteAlways]
    public class BezierTest : MonoBehaviour
    {
        public Transform p0;
        public Transform p1;
        public Transform p2;
        public Transform p3;

        [Range(0, 1)]
        public float t;

        private void Update()
        {
            transform.position = Bezier.GetPoint(p0.position, p1.position, p2.position, p3.position, t);
            transform.rotation = Quaternion.LookRotation(Bezier.GetFirstDerivative(p0.position, p1.position, p2.position, p3.position, t));
        }

        private void OnDrawGizmos()
        {
            int sigmentNumbers = 20;
            Vector2 previousPoint = p0.position;

            for (int i = 0; i < sigmentNumbers; i++) {
                float parameter = (float) i / sigmentNumbers;
                Vector2 point = Bezier.GetPoint(p0.position, p1.position, p2.position, p3.position, parameter);
                Gizmos.DrawLine(previousPoint, point);
                previousPoint = point;
            }
        }
    }
}