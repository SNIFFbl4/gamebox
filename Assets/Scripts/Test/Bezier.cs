﻿using UnityEngine;

namespace GameBoxProject
{
    public static class Bezier
    {
        public static Vector2 GetPoint(Vector2 p0, Vector2 p1, Vector2 p2, float t)
        {
            Vector2 p01 = Vector2.Lerp(p0, p1, t);
            Vector2 p12 = Vector2.Lerp(p1, p2, t);

            Vector2 p012 = Vector2.Lerp(p01, p12, t);

            return p012;
        }

        public static Vector2 GetPoint(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, float t)
        {
            // Vector2 p01 = Vector2.Lerp(p0, p1, t);
            // Vector2 p12 = Vector2.Lerp(p1, p2, t);
            // Vector2 p23 = Vector2.Lerp(p2, p3, t);
            //
            // Vector2 p012 = Vector2.Lerp(p01, p12, t);
            // Vector2 p123 = Vector2.Lerp(p12, p23, t);
            //
            // Vector2 p0123 = Vector2.Lerp(p012, p123, t);
            //
            // return p0123;

            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;

            return 
                    oneMinusT * oneMinusT * oneMinusT * p0 + 
                    3f * oneMinusT * oneMinusT * t * p1 + 
                    3f * oneMinusT * t * t * p2 + 
                    t * t * t * p3;
        }

        public static Vector2 GetFirstDerivative(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3, float t)
        {
            t = Mathf.Clamp01(t);
            float oneMinusT = 1f - t;

            return 
                    3f * oneMinusT * oneMinusT * (p1 - p0) + 
                    6f * oneMinusT * t * (p2 - p1) + 
                    3f * t * t * (p3 - p2);
        }
    }
}