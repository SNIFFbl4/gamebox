﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

namespace Assets.Scripts.Temporary
{
    internal class DOFadeExample : MonoBehaviour
    {
        SpriteRenderer spriteRenderer;

        void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();

            // Плавно изменяем прозрачность спрайта от 0 до 1
            spriteRenderer.DOFade(1f, 1f).OnComplete(ReverseFade);
        }

        void ReverseFade()
        {
            // Плавно изменяем прозрачность спрайта от 1 до 0
            spriteRenderer.DOFade(0f, 1f).SetDelay(1f).OnComplete(Start);
        }
    }

    public class ExampleScript : MonoBehaviour
    {
        public SpriteRenderer spriteRenderer;
        public float duration = 1f;

        private void Start()
        {
            // Начальное значение прозрачности - ноль
            float startAlpha = 0f;

            // Конечное значение прозрачности - 100%
            float endAlpha = 1f;

            // Создаем твин для изменения прозрачности
            var sequence = DOTween.Sequence();
            sequence.Append(spriteRenderer.DOFade(endAlpha, duration));
            sequence.Append(spriteRenderer.DOFade(startAlpha, duration));
            sequence.SetLoops(-1);
        }
    }
}
