using System;

namespace GameBoxProject
{
    public class Timer
    {
        public event Action<int> OnNewMinuteStarted;
        public event Action OnEnemyUpgradeTimerEnded;
        public event Action<string> OnTimeChanged;
        public static event Action OnTimeEnded;

        private int _minutesLeft;
        private float _secondsLeft;

        private float _secondsToUpgradeEnemy;
        private float _backTimer;

        public Timer(int min, float sec, float secToUpgrade)
        {
            _minutesLeft = min;
            _secondsLeft = sec;

            _secondsToUpgradeEnemy = secToUpgrade;
            _backTimer = _secondsToUpgradeEnemy;
        }

        /// <summary>
        /// ����� ��� ��������� ������� � �������
        /// </summary>
        /// <param name="seconds"></param>
        public void DecreaseTime(float seconds)
        {
            _backTimer -= seconds;
            if (_backTimer <= 0)
            {
                _backTimer = _secondsToUpgradeEnemy;
                OnEnemyUpgradeTimerEnded?.Invoke();
            }

            if (_secondsLeft <= seconds)                              //����� ������� ������ ������, ��� �������� �� �������
            {
                if (_minutesLeft == 0)
                {
                    OnTimeEnded?.Invoke();
                    return;
                }

                _minutesLeft--;
                OnNewMinuteStarted?.Invoke(_minutesLeft);

                _secondsLeft = 60;
                _secondsLeft -= seconds;
            }
            else                                                //�� ������� ������ 0 ������
            {
                if (_secondsLeft > seconds)                     //�� ������� ������ ������, ��� ��������� �������
                {
                    _secondsLeft -= seconds;
                }
                else                                            //�� ������� ������ ������, ��� ��������� �������
                {
                    float difference = seconds - _secondsLeft;

                    _minutesLeft--;
                    OnNewMinuteStarted?.Invoke(_minutesLeft);

                    _secondsLeft = 60;
                    _secondsLeft -= difference;
                }
            }

            OnTimeChanged?.Invoke(GetTime());
        }

        public string GetTime()
        {
            string minutesText = _minutesLeft.ToString("00");
            string secondsText = _secondsLeft.ToString("00");
            string timeText = string.Format("{0}:{1}", minutesText, secondsText);
            return timeText;
        }
    }
}