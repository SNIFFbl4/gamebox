﻿using UnityEngine;

namespace Assets.Scripts.Datas
{
    [CreateAssetMenu(fileName = "DamageVignetteData", menuName = "Data/DamageVignette")]
    public class DamageVignetteData : ScriptableObject
    {
        [SerializeField] private float _alphaRiseDuration;      //продолжительность нарастания альфа
        [SerializeField] private float _alphaDecreaseDuration;  //продолжительность убывания альфа
        [SerializeField] private float _alphaMinValue;          //минимальное значение альфа

        public float AlphaRiseDuration => _alphaRiseDuration;
        public float AlphaDecreaseDuration => _alphaDecreaseDuration;
        public float AlphaMinValue => _alphaMinValue;
    }
}
