using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "WordBase")]
public class AllWords : ScriptableObject
{
    public List<WordList> Words;

    public bool HasWord(string word, out WordType type)
    {
        foreach (var wordList in Words)
        {
            if (wordList.Words.Contains(word.ToLower()))
            {
                type = wordList.Type;
                return true;
            }
        }
        type = WordType.unknown;
        return false;
    }

    public static AllWords operator +(AllWords a, AllWords b)
    {
        List<string> nouns = new();
        var nounsA = a.Words.Find(x => x.Type.Equals(WordType.noun))?.Words;
        var nounsB = b.Words.Find(x => x.Type.Equals(WordType.noun))?.Words;
        if (nounsA == null && nounsB == null)
            nouns = null;
        else
            nouns = nounsA.Concat(nounsB).ToList();


        List<string> adj = new();
        var adjA = a.Words.Find(x => x.Type.Equals(WordType.adjective))?.Words;
        var adjB = b.Words.Find(x => x.Type.Equals(WordType.adjective))?.Words;
        if (adjA == null && adjB == null)
            adj = null;
        else
            adj = adjA.Concat(adjB).ToList();


        List<string> verbs = new();
        var verbA = a.Words.Find(x => x.Type.Equals(WordType.verb))?.Words;
        var verbB = b.Words.Find(x => x.Type.Equals(WordType.verb))?.Words;
        if (verbA == null && verbB == null)
            verbs = null;
        else
            verbs = verbA.Concat(verbB).ToList();


        AllWords result = new AllWords();
        result.Words = new List<WordList>();


        if (nouns != null)
        {
            WordList nounWords = new WordList();
            nounWords.Type = WordType.noun;
            nounWords.Words = nouns;
            result.Words.Add(nounWords);
        }

        if (adj != null)
        {
            WordList adjWords = new WordList();
            adjWords.Type = WordType.adjective;
            adjWords.Words = adj;
            result.Words.Add(adjWords);
        }

        if (verbs != null)
        {
            WordList verbWords = new WordList();
            verbWords.Type = WordType.verb;
            verbWords.Words = verbs;
            result.Words.Add(verbWords);
        }

        return result;
    }
}

[System.Serializable]
public class WordList
{
    public WordType Type;
    public List<string> Words;
}

public enum WordType
{
    noun = 0,
    adjective = 1,
    verb = 2,
    pronoun = 3,
    any = 4, 
    other = 5,
    unknown = 6
}